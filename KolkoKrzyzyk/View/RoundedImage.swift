//
//  RoundedImage.swift
//  KolkoKrzyzyk
//
//  Created by Michał Podgórni on 01/12/2018.
//  Copyright © 2018 Michał Podgórni. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedImage: UIImageView {

    override func awakeFromNib() {
        setupView()
    }
    
    func setupView(){
        self.layer.cornerRadius = self.frame.width / 2
        self.clipsToBounds = true
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setupView()
    }

}
