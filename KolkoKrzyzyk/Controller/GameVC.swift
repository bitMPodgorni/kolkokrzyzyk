//
//  ViewController.swift
//  KolkoKrzyzyk
//
//  Created by Michał Podgórni on 01/12/2018.
//  Copyright © 2018 Michał Podgórni. All rights reserved.
//

import UIKit

class GameVC: UIViewController {

    // Outlets
    @IBOutlet weak var viewBtn: UIButton!
    @IBOutlet var gameButtons : [RoundedButton]!
    @IBOutlet weak var modeSwitcher: UISwitch!
    @IBOutlet weak var stopClickBtn: UIView!
    @IBOutlet weak var xCountWins: RoundedButton!
    @IBOutlet weak var oCountWins: RoundedButton!
    
    let mojSzaryKolor = UIColor(displayP3Red: 0.667, green: 0.667, blue: 0.667, alpha: 1).cgColor //(red: 68, green: 160, blue: 141, alpha: 1)
    let winningCombination = [[1,2,3],[4,5,6],[7,8,9],[1,4,7],[2,5,8],[3,6,9],[1,5,9],[3,5,7]]
    var x: Bool = true
    var xWins = 0
    var oWins = 0
    var scoreboard = //0,1
        [0,0,0,
         0,0,0,
         0,0,0]
    var scoreboardX = //0,1...8,9
        [0,0,0,
         0,0,0,
         0,0,0]
    var scoreboardO = //0,1...8,9
        [0,0,0,
         0,0,0,
         0,0,0]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNumberWins()
    }
    
    func setNumberWins() {
        xCountWins.setTitle("\(xWins)", for: .normal)
        oCountWins.setTitle("\(oWins)", for: .normal)
    }
    
    func generatePlayer(){
        let random = arc4random_uniform(2)
        if(random==1){
            x = true
        } else if(random==0) {
            x = false
        } else {
            x = true
        }
    }
    
    @IBAction func playerSwitcherChanged(_ sender: Any) {
        clearAll()
        if(modeSwitcher.isOn){
            x = true
        }
        else if(!modeSwitcher.isOn){
            generatePlayer()
        }
    }
    
    @IBAction func xoBtnBressed(_ sender: RoundedButton) {
        sender.isEnabled = false //btnDisable
        scoreboard(x, sender)
        var isWin = whoWin()
        
        if(!isWin && modeSwitcher.isOn){
            stopClickBtn.isHidden = false
            var status = false
            var countStatus = 0
            while(!status){
                status = autoPlay()
                countStatus += 1
                if(countStatus == 40){ // do usuniecia
                    status = true
                }
            }
            if(status){
                delay(1) {
                    self.changePlayer()
                    isWin = self.whoWin()
                    self.changePlayer()
                }
            }
        } else if(!modeSwitcher.isOn){
            changePlayer()
        }
//        consoleWrite()
    }
    
    func scoreboard(_ isX : Bool,_ roundedButton : RoundedButton) {
        let tagNumber = roundedButton.tag
        if(isX){ // if { x }
            roundedButton.layer.backgroundColor = UIColor.black.cgColor
            scoreboard[tagNumber-1] = 1
            scoreboardX[tagNumber-1] = tagNumber // 1
        } else if(!isX){ // if { o }
            roundedButton.layer.backgroundColor = UIColor.yellow.cgColor
            scoreboard[tagNumber-1] = 1
            scoreboardO[tagNumber-1] = tagNumber // 0
        } else { // if { 'error' }
            roundedButton.layer.backgroundColor = UIColor.red.cgColor
            scoreboard[tagNumber-1] = 0 //9
        }
    }
    
    func whoWin() -> Bool{
        var status = false
        var count = 0
        for i in 0...8 {
            count += scoreboard[i]
        }
//        print(count)
    
        if(isWin(scoreboardX)){
            viewBtn.isHidden = false
//            print("Wygrał X")
            status = true
            xWins += 1
            setNumberWins()
        } else if(isWin(scoreboardO)){
            viewBtn.isHidden = false
//            print("Wygrał Y")
            status = true
            oWins += 1
            setNumberWins()
        } else if(count==9){
            viewBtn.isHidden = false
//            print("Remis")
            status = true
        }
        return status
    }
    
    func isWin(_ sb : [Int]) -> Bool {
        var winStatus = false
        for a in 0...7 {
            var countWin = 0
            for b in 0...2 {
                for c in 0...8 {
                    if(winningCombination[a][b]==sb[c]) {
                        countWin += 1
                        if(countWin==3) {
                            winStatus = true
                        }
                    }
                }
            }
        }
        return winStatus
    }
    
    func changePlayer(){
        if(x==true) {
            x = false
        } else {
            x = true
        }
    }
    
    func autoPlay() -> Bool{
        var status = false
        let randomInt = randomGenerate()
        if(scoreboard[randomInt-1] == 0){
            scoreboard[randomInt-1] = 1
            scoreboardO[randomInt-1] = randomInt
            delay(1) {
                self.gameButtons[randomInt-1].layer.backgroundColor = UIColor.yellow.cgColor
                self.stopClickBtn.isHidden = true
            }
            gameButtons[randomInt-1].isEnabled = false
            status = true
        } else if(scoreboard[randomInt-1] == 1){
            status = false
        }
//        print("To jest liczba random: \(randomInt)")
        return status
    }
    
    func delay(_ delay: Double, closure: @escaping ()->()) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    func randomGenerate() -> Int{
        let random = Int.random(in: 1...9)
        return random
    }
    
    @IBAction func newGameBtnPressed(_ sender: Any) {
        clearAll()
    }
    
    @IBAction func viewPressed(_ sender: Any) {
        clearAll()
        viewBtn.isHidden = true
    }
    
    func clearAll(){
        for button in gameButtons {
            button.layer.backgroundColor = mojSzaryKolor
            button.isEnabled = true
        }
        for i in 0...8 {
            scoreboard[i] = 0
            scoreboardX[i] = 0
            scoreboardO[i] = 0
        }
        xWins = 0
        oWins = 0
        setNumberWins()
        
    }
    
    @IBAction func infoBtnPressed(_ sender: Any) {
        performSegue(withIdentifier: TO_INFO_SEGUE, sender: nil)
    }
    
    func consoleWrite() {
        print("ScoreBoard")
        for i in 0...8 { // print logs
            print((scoreboard[i]), terminator: " ")
            if(i==2){print("")}
            if(i==5){print("")}
        }
        print(" ")
        print("X")
        for i in 0...8 { // print logs
            print((scoreboardX[i]), terminator: " ")
            if(i==2){print("")}
            if(i==5){print("")}
        }
        print(" ")
        print("O")
        for i in 0...8 { // print logs
            print((scoreboardO[i]), terminator: " ")
            if(i==2){print("")}
            if(i==5){print("")}
        }
        print(" ")
        print(" ")
    }
}

